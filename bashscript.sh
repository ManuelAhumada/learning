#!/bash/bin

#Declaracion de variables
string="Tu frase de texto"

cat << EOF
  "A continuacion se va a imprimir una variable:"
  ${string}.
  "Tambien se pueden usar variables del environment:"
  "${USER}."
  "O se pueden usar comandos:"
  $(date)
EOF

function ejemplo {
  ls ~
  return
}

ejemplo

function analizarnum {
  read -p "Insert a number here => " num
  if [ "$num" -gt 0 ]; then
    echo "Your number is a positive."
  elif [ "$num" -lt 0 ]; then
    echo "Your number is a negative."
  elif [ "$num" -eq 0 ]; then
    echo "Your number is 0."
  else 
    echo "Not valid." >&2
  fi
}

analizarnum;

read -p "Insert a regex expression -> " regexp
grep -i "$regexp" /usr/share/dict/words
echo $regexp

sleep 1

echo "You inserted the parameters: "

c=1

while [[ $# -gt 0 ]]; do
  echo "Argument $c -> $1"
  c=$((c+1))
  shift
done
  
read -p "casevar => " casevar
case "$casevar" in
  1) echo "case 1 selected";;
  *) echo "other case selected";;
esac


